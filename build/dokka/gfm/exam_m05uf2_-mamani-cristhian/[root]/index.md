//[exam_m05uf2_MamaniCristhian](../../index.md)/[[root]](index.md)

# Package [root]

## Functions

| Name | Summary |
|---|---|
| [main](main.md) | [jvm]<br>fun [main](main.md)(args: [Array](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-array/index.html)&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)&gt;) |
