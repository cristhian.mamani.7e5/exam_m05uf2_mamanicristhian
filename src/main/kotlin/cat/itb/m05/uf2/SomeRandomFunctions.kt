package cat.itb.m05.uf2

/**
 * Función pública que crea una lista inmutable
 * de números enteros que se genera a partir de dos elementos de entrada.
 * @param[from] número entero (Int)
 * @param[to]  número entero (Int)
 * @return[List]
 */
fun orderedIntListGenerator(from: Int, to: Int): List<Int> {
    val intList = mutableListOf<Int>()
    for (i in from..to) {
        intList.add(i)
    }
    return intList
}

/**
 * Función pública que calcula el valor mínimo de una lista de enteros.
 * @para[intList] lista de números enteros (Int)
 * @return[min] valor mínimo de la lista (Int)
 */
fun minValue(intList: List<Int>): Int {
    var min = 0
    for (i in intList.indices) {
        if (i < min) {
            min = i
        } else if (i == min) {
            min = i
        }
    }
    return min
}

/**
 * Función pública que devuelve las calificaciones de las notas de un usuario
 * @param[grades] número entero (Int)
 * @return[String] diferentes Strings para cada nota.
 */
fun selectMarks(grades: Int): String {
    return when (grades) {
        0 -> "No presentat"
        1, 2, 3, 4 -> "Insuficient"
        5, 6 -> "Suficient"
        7 -> "Notable"
        10 -> "Excelent + MH"
        else -> "No valid"
    }
}

/**
 * Función pública que calcula la posición de una letra contenidad en un texto (String)
 * @param[string] texto (String)
 * @param[char] letra (Char)
 * @return[charPosition] lista de enteros (Int)
 */
fun charPositionsInString(string: String, char: Char): List<Int> {
    val charPosition = mutableListOf<Int>()
    var coincidencies = 0
    for (i in 0 until string.lastIndex) {
        if (string[i] == char) {
            charPosition.add(i)
        }
    }
    return charPosition
}

/**
 * Función pública que calcula la primera vez que aparece una letra en un texto (String)
 * @param[abc] texto (String)
 * @param[b] letra (Char)
 * @return[i] Cuando se encuentra el carácter  (Int)
 * @return -1 cuando no se encuetnra el carácter (Int)
 */
fun firstOccurrencePosition(abc: String, b: Char): Int {
    val pos: MutableList<Int> = mutableListOf()
    for (i in 0 until abc.length) {
        if (abc[i] == b) {
            return i
        }
    }
    return -1
}

/**
 * Fución que calcula la nota redondeada cuando la nota es superior a 5
 * cuando es inferior a 5 la nota no se redondea.
 * @param[grade] nota (Double)
 * @return[grade] nota (Int)
 */
fun note(grade: Double): Int {
    return grade.toInt()
}